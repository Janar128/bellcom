<?php
	$data = $this->Js->get('#XmlSFindForm')->serializeForm(array('isForm' => true, 'inline' => true));
    $this->Js->get('#XmlSFindForm')->event(
      'submit',
      $this->Js->request(
        array('action' => 'findFile'),
        array(
                'update' => '#searchContent',
                'data' => $data,
                'async' => true,    
                'dataExpression'=>true,
                'method' => 'POST'
            )
        )
    );
    echo $this->Form->create('xmls', array('id' => 'XmlSFindForm', 'action' => 'findFile', 'default' => true));
    echo $this->Form->input('file');
    echo $this->Form->end(__('Search'));
    echo $this->Js->writeBuffer();
?>
<div id="searchContent"></div>