<dl>
<?php
	//Loop throught attributes
	foreach ($fields[0] as $field) {
		foreach ($field->attributes() as $name => $value) {
			?>
			<dt><?php echo $name; ?></dt>
			<dd><?php echo ($name == 'date') ? $this->Time->format($value, '%e.%m.%Y %H:%M') : $value; ?>&nbsp;</dd>
			<?php
		};
	}
?>
</dl>