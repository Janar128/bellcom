<?php
App::uses('Xml', 'Utility');

class PagesController extends AppController {
	public $uses = array('File'); //Include file model
	
	public function index() {}
	
	public function install() {
		//Create url to xml folder
		$url = WWW_ROOT . 'files' . DS . 'xml';
		//Edit data in database
		$files = array(
			array(
				'id' => 1580,
				'location' => $url
			),
			array(
				'id' => 1582,
				'location' => $url
			)
		);
		
		//Save fields
		if($this->File->saveMany($files))
			$this->Session->setFlash(__('Database file urls fixed')); //Set notice falsh
		
		//Redirect to index
		return $this->redirect(array('action' => 'index'));
	}
	
	public function findFile() {
		//Disable layout
		$this->layout = false;
		//Only allow ajax access
		$this->request->onlyAllow('ajax');
		
		//Get inserted file id
		$id = $this->request->data['xmls']['file'];
		
		//Check if not empty search
		if(empty($id)) {
			$this->autoRender = false;
			return __('Error! Missing search number');
		}
		
		//Search value from database
		$file = $this->File->findById($id);
		if(empty($file)) {
			$this->autoRender = false;
			return __('File not found!');
		}
		
		//Open with simplexml
		$xml = Xml::build($file['File']['location'] . DS . 'XML_' . $file['File']['id'] . '.xml');
		//Search table name="meeting"
		$fields = $xml->xpath("//table[@name='meeting']");
		
		//Send values to view
		$this->set('fields', $fields[0]->fields);
	}
}
?>