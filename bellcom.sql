-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Okt 27, 2014 kell 08:45 PM
-- Serveri versioon: 5.6.12-log
-- PHP versioon: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `bellcom`
--
CREATE DATABASE IF NOT EXISTS `bellcom` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bellcom`;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1583 ;

--
-- Andmete tõmmistamine tabelile `files`
--

INSERT INTO `files` (`id`, `location`) VALUES
(1580, 'C:\\Users\\Kasutaja\\Hunter\\bellcom\\app\\webroot\\files\\xml'),
(1582, 'C:\\Users\\Kasutaja\\Hunter\\bellcom\\app\\webroot\\files\\xml');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
